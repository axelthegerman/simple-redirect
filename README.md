# Simple Redirect

Mini Sinatra app to redirect any GET request to a target domain - preserving the original path.

## Features

What Simple Redirect does:

- redirect any GET request to $TARGET_DOMAIN
- retains full query string

## Anti Features

What Simple Redirect does NOT do:

- handle any other request methods, e.g. POST, PUT, OPTIONS

# How to run it

1. `bundle install` to install Sinatra gem
2. `TARGET_DOMAIN=example.com ruby server.rb` to run the app locally
3. `curl -v localhost:4567/test?query=parameter`

# Deployment

1. Make sure `TARGET_DOMAIN` is set up properly.
2. Run via `Procfile` or directly with `ruby server.rb`

# frozen_string_literal: true

require 'sinatra'

MOVED_PERMANENTLY = 301
TARGET_DOMAIN = ENV.fetch('TARGET_DOMAIN')

get '/.well-known/status' do
  'OK'
end

get '/*' do |path|
  return 'Do not run this redirect app on the target domain.' if request.host == TARGET_DOMAIN

  query_string = request.query_string.empty? ? nil : request.query_string
  redirect_url = ["https://#{TARGET_DOMAIN}/#{path}", query_string].compact.join('?')
  redirect redirect_url, MOVED_PERMANENTLY
end
